proc color_align {savermsd} {
	mol delrep 0 top

	mol representation NewCartoon
	mol selection "protein and segname P1"
	mol color ColorID 0
	mol addrep top

	mol representation NewCartoon
	mol selection "protein and segname P2"
	mol color ColorID 1
	mol addrep top

	mol representation NewCartoon
	mol selection "protein and segname P3"
	mol color ColorID 2
	mol addrep top

	mol representation NewCartoon
	mol selection "protein and segname P4"
	mol color ColorID 3
	mol addrep top

	mol representation VDW
	mol selection "resname ATP or resname ADP"
	mol color name
	mol addrep top

	if {$savermsd} {
		set outfile [open rmsd.dat w]
	}
	
	set ref [atomselect top "protein and not hydrogen and not resid 1 to 8" frame 0]
	set compare [atomselect top "protein and not hydrogen and not resid 1 to 8"]
	set everything [atomselect top "all"]

	set num_steps [molinfo top get numframes]

	for {set frame 0} {$frame <= $num_steps} {incr frame} {
		$compare frame $frame
		set trans_mat [measure fit $compare $ref]
		$everything frame $frame
		$everything move $trans_mat
		if {$savermsd} {
			puts $outfile "[measure rmsd $compare $ref]"
		}
	}
	if {$savermsd} {
		close $outfile
	}
}
