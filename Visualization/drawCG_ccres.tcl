# colors subdomains of top molecule by subdomains

proc drawCG_ccres {} {

set frametodraw 0

set domain1A "1 to 34 82 to 147 324 to 347"

set domain1B "35 to 81 "

set domain2A "148 to 184 259 to 323"

set domain2B "185 to 258"

set a1 [atomselect top "protein and resid $domain1A"]
set b2 [atomselect top "protein and resid $domain2B"]
set a2 [atomselect top "protein and resid $domain2A"]
set b1 [atomselect top "protein and resid $domain1B"]

$a1 frame $frametodraw
$b2 frame $frametodraw
$a2 frame $frametodraw
$b1 frame $frametodraw

#draw delete all
draw material AOChalky
draw color 0
draw sphere [measure center $a1] radius 5
draw color 3
draw sphere [measure center $b1] radius 5
draw color 1
draw sphere [measure center $a2] radius 5
draw color 20
draw sphere [measure center $b2] radius 5
draw color 2
draw cylinder [measure center $b2] [measure center $a2] radius 1
draw cylinder [measure center $a1] [measure center $a2] radius 1
draw cylinder [measure center $a1] [measure center $b1] radius 1
}