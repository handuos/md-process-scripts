# colors subdomains of top molecule by subdomains
proc color_ccres {} {

set domain1A "1 to 34 82 to 147 324 to 347"

set domain1B "35 to 81 "

set domain2A "148 to 184 259 to 323"

set domain2B "185 to 258"

mol delrep 0 top
mol representation NewCartoon
mol selection "protein and resid $domain1A"
mol material AOChalky
mol color ColorID 0
mol addrep top

mol representation NewCartoon
mol selection "protein and resid $domain1B"
mol material AOChalky
mol color ColorID 3
mol addrep top

mol representation NewCartoon
mol selection "protein and resid $domain2A"
mol material AOChalky
mol color ColorID 1
mol addrep top

mol representation NewCartoon
mol selection "protein and resid $domain2B"
mol material AOChalky
mol color ColorID 20
mol addrep top

[atomselect top "protein and resid $domain1B"] set beta 1
[atomselect top "protein and resid $domain1A"] set beta 2
[atomselect top "protein and resid $domain2A"] set beta 3
[atomselect top "protein and resid $domain2B"] set beta 4
}