% plot time series given nsperframe, and datasets
function plot_time_series(data, data_label, nsperframe, ini_structure, fila_type, neocl_type, segid, reps)

if nargin < 7
    segid = 0;
end

if nargin < 8
    reps = 0;
end

% set up initial figure handles
h = figure();
hold on;
screensize = get(0,'ScreenSize');
set(h, 'Position', [0 screensize(4) / 3 screensize(3) / 1.5 screensize(4) / 3]);
set(h, 'color', 'w');
opts = optimset('Display','off');

window = 300; % get distribution for the last "window" frames of trajectory
w = 1; % bin size for distribution calculation

colors = jet(length(ini_structure) * length(fila_type) * ...
    length(neocl_type) * length(segid) * length(reps));

%set legends here...
subplot(1, 2, 2);
hold on;
for i = 1 : length(ini_structure) * length(fila_type) * ...
        length(neocl_type) * length(segid) * length(reps)
    plot(0, 0, 'color', colors(i, :), 'LineWidth', 2);
end
legend_entries = cell(1, 1);
for i = 1 : length(ini_structure)
    for j = 1 : length(fila_type)
        for k = 1 : length(neocl_type)
            for l = 1 : length(segid)
                for m = 1 : length(reps)
                    new_legend_entry = strcat(ini_structure{i}, '-', fila_type{j}, ...
                        '-', neocl_type{k});
                    if segid(1) > 0
                        new_legend_entry = strcat(new_legend_entry, '-P', num2str(segid(l)));
                        % for testing purposes only --> disp(new_legend_entry);
                    end
                    if reps(1) > 0
                        new_legend_entry = strcat(new_legend_entry, '-rep', num2str(reps(m)));
                    end
                    legend_entries = [legend_entries, ...
                        {new_legend_entry}];
                end
            end
        end
    end
end
legend_entries = legend_entries(2 : end);
legend(legend_entries);

% actual plots!
% plot time series
subplot(1, 2, 1);
hold on;
for i = 1 : size(data, 2)
    curdata = data{i};
    plot(nsperframe * (1 : length(curdata)), ...
        curdata, 'Color', (colors(i, : ) + [0.8 0.8 0.8]) / 1.8);
    plot(nsperframe * (1 : length(curdata)), ...
        smooth(curdata, 20), 'Color', colors(i, : ), 'LineWidth', 2);
end
xlabel('Time (ns)'); 
ylabel(data_label);
set(findall(gcf, '-property', 'FontSize'), 'FontSize', 20);

% plot distribution (and fit distribution)
subplot(1, 2, 2);
hold on;
for i = 1 : size(data, 2)
    curdata = data{i}(end - window : end);
    [n, xout] = hist(curdata, (max(curdata - min(curdata)) / w));
    m = lsqcurvefit(@(z, x) 1 / (sqrt(2 * pi) * z(1)) ...
        * exp(-(1 / 2) * ((x - z(2)) ./ z(1)) .^ 2), ...
        [std(xout) mean(xout)], xout, n / (w * sum(n)), -Inf, Inf, opts);
    plot(xout, n / (w * sum(n)), 'Color', colors(i, : ), ...
        'Marker', '.', 'LineStyle', 'none', 'MarkerSize', 5);
    nn = linspace(min(min(curdata / 2), min(curdata)), ...
        max(2 * max(curdata), max(curdata)), 300);
    plot(nn, 1 / (sqrt(2 * pi) * m(1)) * ...
        exp(-(1 / 2) * ((nn - m(2)) ./ m(1)) .^ 2), ...
        'Color', colors(i, : ), 'LineWidth', 2);
end
xlabel(data_label);
set(findall(gcf, '-property', 'FontSize'), 'FontSize', 20);
