% This is the master script for analyzing the MD results. The user input
% includes selection of initial structures, filament types, neocleotide
% types, repeats, segments, and datasets to plot. Then this scipt loops
% over the selected features and make plots accordingly.

clear all;
close all;
clc;
nsperframe = 0.1;
ini_structure_all = {'4czf', '4cze', '4czl', '4czj'};
fila_type_all = {'1x1', '1x2', '2x1', '2x2'};
neocl_type_all = {'ATP', 'ADP'};
reps_all = [1, 2];
figure_content_all = {'Dihedral', 'Open angle', 'SASA_total', 'SASA_single', 'SASA_nucleo', ...
    'Bending'};

%############ user inputs start ##########
ini_structure_sel_ind = [1];
fila_type_sel_ind = [4];
neocl_type_sel_ind = [1 2];
reps_sel_ind = [1 2];
segid = [1, 2];
figure_content_ind = [5];
%############ user inputs end ############

names = cell(1, length(ini_structure_sel_ind) * length(fila_type_sel_ind) * ...
    length(neocl_type_sel_ind) * length(reps_sel_ind));

ind = 1;
for i = 1 : length(ini_structure_sel_ind)
    for j = 1 : length(fila_type_sel_ind)
        for k = 1 : length(neocl_type_sel_ind)
            for l = 1 : length(reps_sel_ind)
                names{ind} = strcat('../../CcMreB/', ini_structure_all{ini_structure_sel_ind(i)}, ...
                    '_', fila_type_all{fila_type_sel_ind(j)}, '/', ...
                    fila_type_all{fila_type_sel_ind(j)}, ...
                    neocl_type_all{neocl_type_sel_ind(k)}, 'MG/', ...
                    fila_type_all{fila_type_sel_ind(j)}, ...
                    neocl_type_all{neocl_type_sel_ind(k)}, 'MG', ...
                    num2str(reps_all(reps_sel_ind(l))));
                ind = ind + 1;
            end
        end
    end
end

ini_structure = ini_structure_all(ini_structure_sel_ind);
fila_type = fila_type_all(fila_type_sel_ind);
neocl_type = neocl_type_all(neocl_type_sel_ind);
reps = reps_all(reps_sel_ind);

data_aggregated = cell(1, length(names) * length(segid));

% define file names for different features
for f = 1 : length(figure_content_ind)
    switch figure_content_all{figure_content_ind(f)}
        case {'Dihedral', 'Open angle'}
            filename = cell(1, length(segid));
            for i = 1 : length(segid)
                filename{i} = strcat('CG_P', num2str(segid(i)), '.dat');
            end
            for i = 1 : length(names)
                for j = 1 : length(filename)
                    allcenters = load(strcat(names{i}, '/', filename{j}));
                    disp(strcat(names{i}, '/', filename{j}));
                    coors1B = allcenters(:, 1 : 3);
                    coors1A = allcenters(:, 4 : 6);
                    coors2A = allcenters(:, 7 : 9);
                    coors2B = allcenters(:, 10 : 12);
                    
                    [phi, alpha] = cal_dihedral(coors1B, coors1A, coors2A, coors2B);
                    data_aggregated{(i - 1) * length(segid) + j} = phi;
                end
            end
            plot_time_series(data_aggregated, 'Dihedral angle (deg)', nsperframe, ...
                    ini_structure, fila_type, neocl_type, segid, reps);
        case 'SASA_total'
            filename = cell(1, 1); % only one file for total SASA regardless of monomer number
            for i = 1 : 1
                filename{i} = strcat('SASA_total.dat');
            end
            for i = 1 : length(names)
                for j = 1 : length(filename)
                    SASA = load(strcat(names{i}, '/', filename{j}));
                    disp(strcat(names{i}, '/', filename{j}));

                    data_aggregated{(i - 1) * length(segid) + j} = SASA;
                end
            end
            plot_time_series(data_aggregated, 'Total SASA (A^2)', nsperframe, ...
                    ini_structure, fila_type, neocl_type, 0, reps);
        case 'SASA_single'
            filename = cell(1, length(segid));
            for i = 1 : length(segid)
                filename{i} = strcat('SASA_P', num2str(segid(i)), '.dat');
            end
            for i = 1 : length(names)
                for j = 1 : length(filename)
                    SASA = load(strcat(names{i}, '/', filename{j}));
                    disp(strcat(names{i}, '/', filename{j}));

                    data_aggregated{(i - 1) * length(segid) + j} = SASA;
                end
            end
            plot_time_series(data_aggregated, 'SASA of monomer (A^2)', nsperframe, ...
                    ini_structure, fila_type, neocl_type, segid, reps);
        case 'SASA_nucleo'
            filename = cell(1, length(segid));
            for i = 1 : length(segid)
                filename{i} = strcat('SASA_nucleotide_P', num2str(segid(i)), '.dat');
            end
            for i = 1 : length(names)
                for j = 1 : length(filename)
                    SASA = load(strcat(names{i}, '/', filename{j}));
                    disp(strcat(names{i}, '/', filename{j}));

                    data_aggregated{(i - 1) * length(segid) + j} = SASA;
                end
            end
            plot_time_series(data_aggregated, 'SASA of neocleotide (A^2)', nsperframe, ...
                    ini_structure, fila_type, neocl_type, segid, reps);
        case 'Bending'
            %xxx
    end
end
% 
% filename = {'CG_P1.dat'};
% % filename = {'CG_P1.dat', 'CG_P2.dat'};
% 
% 
% 
% for l = 1 : (length(names) * length(filename))
%     segnum = length(filename);   
%     % Load monomer stuff
%     allcenters = load([names{ceil(l / segnum)}, '/', filename{mod(l - 1, segnum) + 1}]);
%     [names{ceil(l / segnum)}, '/', filename{mod(l - 1, segnum) + 1}]
% 
%     coors1B = allcenters(:,1:3);
%     coors1A = allcenters(:,4:6);
%     coors2A = allcenters(:,7:9);
%     coors2B = allcenters(:,10:12);
%     
%     [phi, alpha] = cal_dihedral(coors1B, coors1A, coors2A, coors2B);
%     data_aggregated = [data_aggregated, {phi}];   
% end
% data_aggregated = data_aggregated(2 : end);
% 
% 
