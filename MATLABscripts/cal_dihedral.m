% input: four vectors, the coordinates of 1B, 1A, 2A, 2B
% output: a vector with the calculated dihedral angles and the open angles

function [dihedral, open] = cal_dihedral(coors1B, coors1A, coors2A, coors2B)

% get bonds between pairs of domains:
bond1B1A = sum((coors1B - coors1A).^2, 2).^ 0.5;
bond1A2A = sum((coors1A - coors2A).^2, 2).^ 0.5;
bond2A2B = sum((coors2A - coors2B).^2, 2).^ 0.5;
bond2B1B = sum((coors2B - coors1B).^2, 2).^ 0.5;

% get vectors between domains
b1 = coors1B - coors1A;
b2 = coors1A - coors2A;
b3 = coors2A - coors2B;

% get alpha1 and alpha2 open angles
alpha2 = acos(dot(b1, -b2, 2) ./ bond1B1A ./ bond1A2A) * 180 / pi;
alpha1 = acos(dot(b2, -b3, 2) ./ bond1A2A ./ bond2A2B) * 180 / pi;
alpha = acos(dot(b1, -b3, 2) ./ bond1B1A ./ bond2A2B) * 180 / pi;

% find cross terms
cb1b2 = cross(b1, b2, 2);
cb2b3 = cross(b2, b3, 2);
nb1b2 = sum(cb1b2 .^ 2, 2).^ 0.5;
nb2b3 = sum(cb2b3 .^ 2, 2).^ 0.5;

% calculate dihedral angle
phi = acos(dot(cb1b2, cb2b3, 2) ./ nb1b2 ./ nb2b3) * 180 / pi;
% flip sign of dihedral so that it's consistent across the simulation 
% (sometimes it can flip)
vn = cross(cb1b2./repmat(nb1b2,1,3),b2,2);
phi(dot(vn, cb2b3, 2) < 0) = - phi(dot(vn, cb2b3, 2) < 0);
dihedral = -1 * phi;
open = (alpha1 + alpha2) ./ 2;