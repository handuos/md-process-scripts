# Created by Handuo Shi 05/04/2015
# input: sel: the selection that to be calculated for SASA
# output: SASA.dat, the SASA at each time point

proc SASA {sel outname} {

    # the srad for sasa calculation
    # default using 1.4 for water: http://www.ccp4.ac.uk/newsletters/newsletter38/03_surfarea.html
    set srad 1.4

    set nf [molinfo top get numframes]
    
    set outfile [open $outname.dat w]

    for {set frame 0} {$frame <= $nf} {incr frame} {
    	$sel frame $frame
    	puts $outfile "[measure sasa $srad $sel]"
    }
    close $outfile
}
