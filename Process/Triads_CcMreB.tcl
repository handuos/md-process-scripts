# Written by Handuo Shi 05/29/2015
# For every single monomer in CcMreB, calculate the principal axis
# sel: selection of a monomer
proc Triads_CcMreB {sel outfilename} {

    source /Users/handuo/Documents/Research/HuangLab/MD/scripts/Rotation/vexpr.tcl
    package require vexpr 1.1
    namespace import vexpr::*
    
	source /Users/handuo/Documents/Research/HuangLab/MD/scripts/Process/FindTriadSingleFrame_Inertia.tcl

	set outfile [open $outfilename.dat w]
	puts "$outfilename"

    set mid [$sel molid]
    set nf [molinfo $mid get numframes] 

    set selc [atomselect $mid [$sel text] frame 0]

    for {set frame 0} {$frame < $nf} {incr frame} {
    	$sel frame $frame
    	$selc move [measure fit $selc $sel]
    	set CM [measure center $selc]
    	set d [FindTriadSingleFrame_Inertia $selc $mid]
    	puts $outfile "$CM $d"
    	# plots the axis
    	if {0} {
    		set pointd1 [vecadd $CM [vecscale [lindex $d 0] 35]]
	    	set pointd2 [vecadd $CM [vecscale [lindex $d 1] 35]]
	    	set pointd3 [vecadd $CM [vecscale [lindex $d 2] 35]]
	    	graphics $mid color blue2
		    graphics $mid cylinder $CM $pointd1 radius 1.0 resolution 60 filled yes
		    graphics $mid color red
		    graphics $mid cylinder $CM $pointd2 radius 1.0 resolution 60 filled yes
		    graphics $mid color orange
		    graphics $mid cylinder $CM $pointd3 radius 1.0 resolution 60 filled yes
    	}
    }
    close $outfile
}