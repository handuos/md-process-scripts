# find the three principal axis for the crystal structure of CcMreB


proc FindTriadSingleFrame_Inertia {sel mid} {

  source /Users/handuo/Documents/Research/HuangLab/MD/scripts/Process/vexpr.tcl
  package require vexpr 1.1
  namespace import vexpr::*

  # Measure center of mass, Principal Axes and eigenvalues
  set inert [measure inertia $sel -eigenvals]

  #set ctheta1 -0.4312146614756282
  #set ctheta2 -0.9022493891105686

  set CA340 [atomselect $mid "[$sel text] and resid 340 and name CA"]
  set CA347 [atomselect $mid "[$sel text] and resid 347 and name CA"]
  set CA67 [atomselect $mid "[$sel text] and resid 67 and name CA"]
  set CA71 [atomselect $mid "[$sel text] and resid 71 and name CA"]
  set CM340 [measure center $CA340]
  set CM347 [measure center $CA347]
  set CM67 [measure center $CA67]
  set CM71 [measure center $CA71]
  set dir3 [vexpr $CM340 - $CM347]
  set dir2 [vexpr $CM71 - $CM67]

  # Get the eigenvalues
  set eva [expr abs([lindex [lindex $inert 2] 0])]
  set evb [expr abs([lindex [lindex $inert 2] 1])]
  set evc [expr abs([lindex [lindex $inert 2] 2])]

  # Sort Eigenvalues.
  # ev1 is the main axis as it is the easiest to rotate around
  set ev1 [lsearch -regexp [lindex $inert 2] [expr min($eva,$evb,$evc)] ]
  set ev3 [lsearch -regexp [lindex $inert 2] [expr max($eva,$evb,$evc)] ]

  if { ($ev1 == 1 && $ev3 == 0) || ($ev1 == 0 && $ev3 == 1) } { set ev2 2 }
  if { ($ev1 == 1 && $ev3 == 2) || ($ev1 == 2 && $ev3 == 1) } { set ev2 0 }
  if { ($ev1 == 0 && $ev3 == 2) || ($ev1 == 2 && $ev3 == 0) } { set ev2 1 }

  set vec10 [lindex [lindex $inert 1] $ev1]
  set vec20 [lindex [lindex $inert 1] $ev2]
  set vec3 [lindex [lindex $inert 1] $ev3]


  if { [vexpr $dir3 . $vec3] < 0 } { set vec3 [vexpr -1 * $vec3] }
  if { [vexpr $dir2 . $vec20] < 0 } { set vec20 [vexpr -1 * $vec20] }
  set dir1 [vexpr $vec3 X $vec20]
  if { [vexpr $dir1 . $vec10] < 0 } { set vec10 [vexpr -1 * $vec10] }

  set vec1 [vecnorm $vec10] 
  set vec2 [vecnorm $vec20] 
  set vec3 [vecnorm $vec3]

  #set vec1 [vexpr [vexpr $ctheta1 * $vec10] + [vexpr $ctheta2 * $vec20]]
  #set vec2 [vexpr [vexpr $ctheta2 * $vec10] - [vexpr $ctheta1 * $vec20]]
  
  set d1 $vec1
  set d2 $vec2
  set d3 $vec3

  return [list $d1 $d2 $d3]
}

