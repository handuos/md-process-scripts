
proc CG_tmari_mono {fname} {

    # This file does coarse grain analysis, and outputs it to fname

    # Filename format:
    #    columns: 
    
    set domain1A "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 315 316 317 318 319 320 321 322 323 324 325 326 327 328 329 330 331 332 333 334 335 336"
    
    set domain1B "30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72"
    
    set domain2A "139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 250 251 252 253 254 255 256 257 258 259 260 261 262 263 264 265 266 267 268 269 270 271 272 273 274 275 276 277 278 279 280 281 282 283 284 285 286 287 288 289 290 291 292 293 294 295 296 297 298 299 300 301 302 303 304 305 306 307 308 309 310 311 312 313 314"
    
    set domain2B "176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249"

    set P1 [atomselect top "segname P1 and name CA"]


    
    set ca1_1A [atomselect top "segname P1 and name CA and protein and resid $domain1A"]
    set ca1_1B [atomselect top "segname P1 and name CA and protein and resid $domain1B"]
    set ca1_2A [atomselect top "segname P1 and name CA and protein and resid $domain2A"]
    set ca1_2B [atomselect top "segname P1 and name CA and protein and resid $domain2B"]
#    set all2_2B [atomselect top "segname P1 and protein and resid $domain2B"]

    $ca1_1A set beta 1
    $ca1_1B set beta 2
    $ca1_2A set beta 3
    $ca1_2B set beta 4


    set mid [$ca1_1A molid]
    set nf [molinfo $mid get numframes]

    set all [atomselect top all]
    set outfile [open $fname w]

    for {set l 0} {$l < $nf} {incr l} {
        $all frame $l
        $ca1_2B frame $l
        $ca1_2A frame $l
        $ca1_1B frame $l
        $ca1_1A frame $l
        puts $outfile "[measure center $ca1_1B] [measure center $ca1_1A] [measure center $ca1_2A] [measure center $ca1_2B]"

    }
    close $outfile
}
