
proc CG_ccres_d1to8 {segnum outname} {

    # This file does coarse grain analysis, and outputs it to file outname
    # prints the coordinates of four subunits in mreB.
    # remove residues 1 to 8 (unstable alpha helix)

    # Filename format:
    #    columns: 


    set domain1A "9 to 34 82 to 147 324 to 347"

    set domain1B "35 to 81 "

    set domain2A "148 to 184 259 to 323"

    set domain2B "185 to 258"

    for {set seg 1} {$seg <= $segnum} {incr seg} {

        set P1 [atomselect top "protein and name CA"]


    
        set ca1_1A [atomselect top "name CA and protein and resid $domain1A and segname P$seg"]
        set ca1_1B [atomselect top "name CA and protein and resid $domain1B and segname P$seg"]
        set ca1_2A [atomselect top "name CA and protein and resid $domain2A and segname P$seg"]
        set ca1_2B [atomselect top "name CA and protein and resid $domain2B and segname P$seg"]

        $ca1_1A set beta 1
        $ca1_1B set beta 2
        $ca1_2A set beta 3
        $ca1_2B set beta 4


        set mid [$ca1_1A molid]
        set nf [molinfo $mid get numframes]

        set all [atomselect top all]
        set outfilename $outname
        append outfilename _P
        append outfilename $seg
        append outfilename .dat
        set outfile [open $outfilename w]

        for {set l 0} {$l < $nf} {incr l} {
            $all frame $l
            $ca1_2B frame $l
            $ca1_2A frame $l
            $ca1_1B frame $l
            $ca1_1A frame $l
            puts $outfile "[measure center $ca1_1B] [measure center $ca1_1A] [measure center $ca1_2A] [measure center $ca1_2B]"

        }
        close $outfile
    }
}
