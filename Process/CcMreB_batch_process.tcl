# Created by Handuo Shi 05/04/2015
# Go through all folders and run the analysis for the selected files
# all directories are hard-coded!!!

mol delete all

# this is to reduce memory in test
set step 1
# for test purpose only --> set step 500
# maximum number of segments
set maxsegnum 4

source /Users/handuo/Documents/Research/HuangLab/MD/scripts/Visualization/color_align.tcl
source /Users/handuo/Documents/Research/HuangLab/MD/scripts/Process/SASA.tcl
source /Users/handuo/Documents/Research/HuangLab/MD/scripts/Process/CG_ccres_d1to8.tcl
source /Users/handuo/Documents/Research/HuangLab/MD/scripts/Process/Triads_CcMreB.tcl


set scriptdir /Users/handuo/Documents/Research/HuangLab/MD/scripts/Process/

set maindirectory /Users/handuo/Documents/Research/HuangLab/MD/CcMreB/
# for test purpose only --> set subdir [list 4czf_2x2/2x2ATPMG/2x2ATPMG1 4czf_1x1/1x1ATPMG/1x1ATPMG1]
#set subdir [list 4czf_1x1/1x1ATPMG/1x1ATPMG2 4czf_1x1/1x1ADPMG/1x1ADPMG2 \
	4czf_1x2/1x2ATPMG/1x2ATPMG2 4czf_1x2/1x2ADPMG/1x2ADPMG2 \
	4czf_2x1/2x1ATPMG/2x1ATPMG2 4czf_2x1/2x1ADPMG/2x1ADPMG2 \
	4czf_2x2/2x2ATPMG/2x2ATPMG2 4czf_2x2/2x2ADPMG/2x2ADPMG2]
#set subdir [list 4czf_1x1/1x1ATPMG/1x1ATPMG2 4czf_1x1/1x1ADPMG/1x1ADPMG2 \
	4czf_1x2/1x2ATPMG/1x2ATPMG2 4czf_1x2/1x2ADPMG/1x2ADPMG2 \
	4czf_2x1/2x1ATPMG/2x1ATPMG3 4czf_2x1/2x1ADPMG/2x1ADPMG2 \
	4czf_2x2/2x2ATPMG/2x2ATPMG2 4czf_2x2/2x2ADPMG/2x2ADPMG2 \
	4czf_1x1/1x1ATPMG/1x1ATPMG1 4czf_1x1/1x1ADPMG/1x1ADPMG1 \
	4czf_1x2/1x2ATPMG/1x2ATPMG1 4czf_1x2/1x2ADPMG/1x2ADPMG1 \
	4czf_2x1/2x1ATPMG/2x1ATPMG1 4czf_2x1/2x1ADPMG/2x1ADPMG1 \
	4czf_2x2/2x2ATPMG/2x2ATPMG1 4czf_2x2/2x2ADPMG/2x2ADPMG1]
#set subdir [list 4cze_1x1/1x1ATPMG/1x1ATPMG1 4cze_1x1/1x1ADPMG/1x1ADPMG1 \
	4czl_1x1/1x1ATPMG/1x1ATPMG1 4czl_1x1/1x1ADPMG/1x1ADPMG1]
set subdir [list 4czj_1x2/1x2ADPMG/1x2ADPMG1]

foreach dir $subdir {
	mol delete all
	set fulldir $maindirectory$dir
	puts $fulldir
	cd $fulldir
	mol load pdb ionized.pdb psf ionized.psf
	mol addfile [glob all*s50.dcd] step $step waitfor all

	# align trajectory and save the RMSD
	if {1} {
		color_align 1
	}

	# save CoM for each segment
	if {1} {
		set totalseg [expr $maxsegnum+1]
		for {set seg 1} {$seg <= $maxsegnum} {incr seg} {
			set sel [atomselect top "protein and resid 9 to 347 and segname P$seg"]
			if {[$sel num] == 0} {
				set totalseg $seg
				break
			}
		}
		set totalseg [expr $totalseg-1]
		puts "$totalseg"
		CG_ccres_d1to8 $totalseg CG
	}

	# save SASA for each segment
	if {1} {
		for {set seg 1} {$seg <= $maxsegnum} {incr seg} {
			set sel [atomselect top "protein and resid 9 to 347 and segname P$seg"]
			if {[$sel num] > 0} {
				SASA $sel SASA_P$seg
				puts "P$seg"
			}
		}
	}

	# save SASA for whole system
	if {1} {
		set sel [atomselect top "protein and resid 9 to 347"]
		SASA $sel SASA_total
	}

	# save SASA for ATP/ADP in each segment
	if {1} {
		for {set seg 1} {$seg <= $maxsegnum} {incr seg} {
			set nseg [expr $seg*2]
			set sel [atomselect top "resname ATP and segname O$nseg or resname ADP and segname O$nseg"]
			if {[$sel num] > 0} {
				SASA $sel SASA_nucleotide_P$seg
				puts "O$nseg"
				puts [$sel num]
			}
		}
	}

	# save triads for each
	if {1} {
		for {set seg 1} {$seg <= $maxsegnum} {incr seg} {
			set sel [atomselect top "protein and resid 9 to 347 and segname P$seg"]
			if {[$sel num] > 0} {
				Triads_CcMreB $sel Triads_P$seg
				puts "$seg"
			}
		}
	}

	cd $scriptdir
}