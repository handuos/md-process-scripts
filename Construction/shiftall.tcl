proc shiftall {shift} {

set newNumbering []
set mySel [atomselect top all]
set oldNumbering [$mySel get resid]
foreach resid $oldNumbering {
	lappend newNumbering [expr {$resid + $shift}]
}

$mySel set resid $newNumbering
}
