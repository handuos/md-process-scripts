resetpsf
package require psfgen
topology ./top_all36_prot.rtf

pdbalias residue HIS HSD
#pdbalias atom ILE CD1 CD

## renumbering
mol load pdb moved_4czf.pdb
set all [atomselect top "all and noh"]
$all writepdb OneMreB-noh.pdb
mol delete top

# Make sure no atoms have identical positions
if {1} {
    mol load pdb OneMreB-noh.pdb
    set allca [atomselect top "name CA"]
    set numca [$allca num]
    for {set i 1} {$i <= $numca} {incr i} {
        set curatoms [atomselect top "protein and resid $i"]
        set curind [$curatoms get index]
        for {set j 0} {$j < [llength $curind]} {incr j} {
            set thisatom [atomselect top "index [lindex $curind $j]"]
            set thisxyz [$thisatom get {x y z}]
            for {set k [expr $j+1]} {$k < [llength $curind]} {incr k} {
                
                set thatatom [atomselect top "index [lindex $curind $k]"]
                set thatxyz [$thatatom get {x y z}]
                set dist 0
                for {set l 0} {$l < 3} {incr l} {
                    set dist [expr $dist + [expr pow([lindex $thisxyz 0 $l]-[lindex $thatxyz 0 $l],2)]]

                }
                set dist [expr pow($dist,0.5)]
                if {$dist < .02} {
                    $thatatom set x [expr [lindex $thatxyz 0 0] + .02]
                    $thatatom set y [expr [lindex $thatxyz 0 1] + .02]
                    $thatatom set z [expr [lindex $thatxyz 0 2] + .02]

                }
            }
        }
    }
    
}
set a [atomselect top all]
$a writepdb OneMreB-noh-moved.pdb
mol delete top



# Writeout consecutive residue chunks
if {1} {
    mol load pdb OneMreB-noh-moved.pdb
    set sel [atomselect top "resid 9 to 43"]
    $sel writepdb ChunkE9to43.pdb
    set sel [atomselect top "resid 48 to 225"]
    $sel writepdb ChunkF48to225.pdb
    set sel [atomselect top "resid 230 to 345"]
    $sel writepdb ChunkG230to345.pdb
    mol delete top
}


# Now stitch everything back together.
if {1} {
  ## segname P1
  segment P1 {
    pdb moved_alpha.pdb   
  }
  coordpdb moved_alpha.pdb P1

  ## segname P2
  segment P2 {
    pdb ChunkE9to43.pdb
  }
  coordpdb ChunkE9to43.pdb P2

  ## segname P3
  segment P3 {
    pdb moved_turn.pdb
  }
  coordpdb moved_turn.pdb P3

  
  ## segname P3
  segment P4 {
    pdb ChunkF48to225.pdb
    mutate 102 PHE
    mutate 103 VAL
  }
  coordpdb ChunkF48to225.pdb P4

  ## segname P4
  segment P5 {
    pdb moved_straight2.pdb   
  }
  coordpdb moved_straight2.pdb P5

  ## segname P6
  segment P6 {
    pdb ChunkG230to345.pdb  
  }
  coordpdb ChunkG230to345.pdb P6

  ## segname P7
  segment P7 {
    pdb moved_straight3.pdb  
  }
  coordpdb moved_straight3.pdb P7

  guesscoord
  writepdb OneMreB-final.pdb
}

resetpsf
