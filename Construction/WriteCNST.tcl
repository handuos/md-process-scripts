# set $sel beta value to 1, and save top into filename
proc WriteCNST {sel filename} {
	set all [atomselect top all]
	$all set beta 0

	$sel set beta 1
	set n [$sel num]
	puts "The selection contains $n atom(s)"
	$all writepdb $filename
	puts "File written to $filename"
}

