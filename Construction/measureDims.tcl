# measure the dimensions for current atoms
proc measureDims {} {
	set a [atomselect top "all"]

	set mins [measure minmax $a]
	set x [expr abs([lindex $mins 0 0] - [lindex $mins 1 0])]
	set y [expr abs([lindex $mins 0 1] - [lindex $mins 1 1])]
	set z [expr abs([lindex $mins 0 2] - [lindex $mins 1 2])]
	set c [measure center $a]
	puts "cellBasisVector1   $x   0.   0."
	puts "cellBasisVector2   0.   $y   0."
	puts "cellBasisVector3   0.   0.   $z"
	puts "cellOrigin   $c"
}
